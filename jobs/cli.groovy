multibranchPipelineJob('gitlab-dsl-job') {
  branchSources {
    git {
      // must not be empty but unique per branchSource
      id('gitlab-dsl-job')
      remote('https://gitlab.com/andrei.radulescu1/gitlab-dsl-job.git')
      credentialsId('gitlab')
      //buildOriginBranchWithPR(false)
      //buildOriginPRMerge(true)
      //buildForkPRMerge(true)
    }
  }
  orphanedItemStrategy {
    discardOldItems {
      numToKeep(1)
    }
  }
}
